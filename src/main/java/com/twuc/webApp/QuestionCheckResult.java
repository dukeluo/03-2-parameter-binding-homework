package com.twuc.webApp;

public class QuestionCheckResult {
    private Boolean correct;

    public QuestionCheckResult(Boolean correct) {
        this.correct = correct;
    }

    public QuestionCheckResult() {
    }

    public Boolean getCorrect() {
        return correct;
    }
}
