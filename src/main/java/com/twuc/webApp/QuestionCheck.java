package com.twuc.webApp;

import javax.validation.constraints.NotNull;

public class QuestionCheck {
    @NotNull
    private Integer expectedResult;

    @NotNull
    private Integer operandLeft;

    @NotNull
    private Integer operandRight;

    @NotNull
    private String operation;

    private String checkType;

    public QuestionCheck(Integer operandLeft, Integer operandRight, String operation, Integer expectedResult, String checkType) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
        this.expectedResult = expectedResult;
        this.checkType = checkType;
    }

    public QuestionCheck() {
    }

    public Integer getExpectedResult() {
        return expectedResult;
    }

    public Integer getOperandLeft() {
        return operandLeft;
    }

    public Integer getOperandRight() {
        return operandRight;
    }

    public String getOperation() {
        return operation;
    }

    public String getCheckType() {
        return checkType;
    }

    public QuestionCheckResult check() {
        Integer actualResult = null;

        if (operation.equals("*")) {
            actualResult = operandLeft * operandRight;
        } else if (operation.equals("+")) {
            actualResult = operandLeft + operandRight;
        }
        if (checkType == null) {
            return new QuestionCheckResult(actualResult.equals(expectedResult));
        }
        if (checkType.equals(">")) {
            return new QuestionCheckResult(actualResult > expectedResult);
        } else if (checkType.equals("<")) {
            return new QuestionCheckResult(actualResult < expectedResult);
        } else if (checkType.equals("=")) {
            return new QuestionCheckResult(actualResult.equals(expectedResult));
        }
        return new QuestionCheckResult(actualResult.equals(expectedResult));
    }
}
