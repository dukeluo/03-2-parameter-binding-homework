package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class UserController {
    @GetMapping("/api/tables/plus")
    public String getAdditionTable(@RequestParam(defaultValue = "1") int start, @RequestParam(defaultValue = "9") int end) {
        if (start > end) {
            throw new RuntimeException("start is greater than end");
        }
        return new MathOperationTable("add").createTable(start, end);
    }

    @GetMapping("/api/tables/multiply")
    public String getMultiplicationTable(@RequestParam(defaultValue = "1") int start, @RequestParam(defaultValue = "9") int end) {
        if (start > end) {
            throw new RuntimeException("start is greater than end");
        }
        return new MathOperationTable("mul").createTable(start, end);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> runtimeExceptionHandler(RuntimeException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(exception.getMessage());
    }

    @PostMapping("/api/check")
    public QuestionCheckResult getDate(@RequestBody @Valid QuestionCheck check) {
        return check.check();
    }
}
