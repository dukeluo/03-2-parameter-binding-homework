package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MathOperationTableTest {

    @Test
    void should_return_addition_table_when_operation_string_is_add() {
        //given
        String operationString = "add";
        String expected = "1+1=2\n" +
                "2+1=3  2+2=4\n" +
                "3+1=4  3+2=5  3+3=6\n" +
                "4+1=5  4+2=6  4+3=7  4+4=8\n" +
                "5+1=6  5+2=7  5+3=8  5+4=9  5+5=10\n" +
                "6+1=7  6+2=8  6+3=9  6+4=10 6+5=11 6+6=12\n" +
                "7+1=8  7+2=9  7+3=10 7+4=11 7+5=12 7+6=13 7+7=14\n" +
                "8+1=9  8+2=10 8+3=11 8+4=12 8+5=13 8+6=14 8+7=15 8+8=16\n" +
                "9+1=10 9+2=11 9+3=12 9+4=13 9+5=14 9+6=15 9+7=16 9+8=17 9+9=18\n";

        //when
        MathOperationTable table = new MathOperationTable(operationString);

        //then
        assertEquals(expected, table.createTable(1, 9));
    }

    @Test
    void should_return_multiplication_table_when_operation_string_is_mul() {
        //given
        String operationString = "mul";
        String expected = "1*1=1\n" +
                "2*1=2 2*2=4\n" +
                "3*1=3 3*2=6  3*3=9\n" +
                "4*1=4 4*2=8  4*3=12 4*4=16\n" +
                "5*1=5 5*2=10 5*3=15 5*4=20 5*5=25\n" +
                "6*1=6 6*2=12 6*3=18 6*4=24 6*5=30 6*6=36\n" +
                "7*1=7 7*2=14 7*3=21 7*4=28 7*5=35 7*6=42 7*7=49\n" +
                "8*1=8 8*2=16 8*3=24 8*4=32 8*5=40 8*6=48 8*7=56 8*8=64\n" +
                "9*1=9 9*2=18 9*3=27 9*4=36 9*5=45 9*6=54 9*7=63 9*8=72 9*9=81\n";

        //when
        MathOperationTable table = new MathOperationTable(operationString);

        //then
        assertEquals(expected, table.createTable(1, 9));
    }
}