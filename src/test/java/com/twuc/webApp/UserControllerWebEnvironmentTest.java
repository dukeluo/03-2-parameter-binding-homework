package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class UserControllerWebEnvironmentTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_handler_the_exception_when_the_start_is_greater_than_end() {
        ResponseEntity<String> entity = testRestTemplate.getForEntity("/api/tables/plus?start=5&end=3", String.class);

        assertEquals(400, entity.getStatusCodeValue());
        assertEquals("start is greater than end", entity.getBody());
    }
}